module Grid exposing
    ( Grid
    , filterToList
    , indexedFilterToList
    , indexedMap
    , map
    , map2
    , reverseCols
    , set
    , sum
    , transpose
    )

import Recurse exposing (Recursion(..), recurse)


type alias Grid a =
    List (List a)


map : (a -> b) -> Grid a -> Grid b
map f =
    List.map (List.map f)


map2 : (a -> b -> c) -> Grid a -> Grid b -> Grid c
map2 f =
    List.map2 (List.map2 f)


sum : Grid number -> number
sum grid =
    List.sum (List.map List.sum grid)


any : (a -> Bool) -> Grid a -> Bool
any f grid =
    List.any identity (List.map (List.any f) grid)


indexedMap : (Int -> Int -> a -> b) -> Grid a -> Grid b
indexedMap f =
    List.indexedMap
        (\colNum col ->
            List.indexedMap (\rowNum item -> f colNum rowNum item) col
        )


filterToList : (a -> Maybe b) -> Grid a -> List b
filterToList f grid =
    grid |> List.concat |> List.filterMap f


indexedFilterToList : (Int -> Int -> a -> Maybe b) -> Grid a -> List b
indexedFilterToList f grid =
    indexedMap (\col row item -> ( col, row, item )) grid
        |> List.concat
        |> List.filterMap (\( col, row, item ) -> f col row item)


set : Int -> Int -> a -> Grid a -> Grid a
set colNum rowNum newValue =
    indexedMap
        (\testColNum testRowNum value ->
            if testColNum == colNum && testRowNum == rowNum then
                newValue

            else
                value
        )


transpose : Grid a -> Grid a
transpose grid =
    case grid of
        col0 :: _ ->
            recurse
                ( grid, List.repeat (List.length col0) [] )
                (\( remainingCols, result ) ->
                    case remainingCols of
                        col :: newRemainingCols ->
                            Recurse
                                ( newRemainingCols
                                , List.map2 (::) col result
                                )

                        [] ->
                            Stop (reverseCols result)
                )

        [] ->
            []


reverseCols : Grid a -> Grid a
reverseCols =
    List.map List.reverse
