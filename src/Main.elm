port module Main exposing (main)

import Browser
import Browser.Events exposing (onAnimationFrameDelta, onKeyDown)
import Color exposing (Color)
import Color.Interpolate as CI
import Color.Manipulate as CM
import Color.Oklch as Oklch
import Grid exposing (Grid)
import Html exposing (Html)
import Html.Attributes
import Html.Events.Extra.Touch as Touch
import Json.Decode as Decode
import Json.Encode as Encode
import Layout
import Random
import Recurse exposing (Recursion(..), recurse)
import String.Interpolate exposing (interpolate)
import TypedSvg.Core exposing (Svg)
import TypedSvg.Types exposing (..)



-- MAIN


main : Program Encode.Value Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }



-- MODEL


type alias Model =
    { boardState : BoardState
    , score : Int
    , highScore : Int
    , gameIsOver : Bool
    , infoIsBeingShown : Bool
    , touchModel : Maybe TouchModel
    }


type BoardState
    = Static { tiles : Grid (Maybe Int) }
    | Sliding SlidingBoardState


type alias SlidingBoardState =
    { tiles : Grid (Maybe SlidingTile)
    , amount : Float
    , step : Int
    , direction : Direction
    }


type alias SlidingTile =
    { rank : Int
    , didCombine : Bool
    }


type Direction
    = Left
    | Right
    | Up
    | Down


type alias TouchModel =
    { identifier : Int
    , position : ( Float, Float )
    }


init : Encode.Value -> ( Model, Cmd Msg )
init flags =
    let
        highScore =
            Result.withDefault 0
                (Decode.decodeValue
                    (Decode.field "highScore" Decode.int)
                    flags
                )
    in
    ( { boardState = Static { tiles = initTiles }
      , score = 0
      , highScore = highScore
      , gameIsOver = False
      , infoIsBeingShown = False
      , touchModel = Nothing
      }
    , newTileCmd initTiles
    )


initTiles : Grid (Maybe Int)
initTiles =
    List.repeat numRows (List.repeat numRows Nothing)


numRows : Int
numRows =
    4



-- PORTS


port setStorage : Encode.Value -> Cmd msg



-- UPDATE


type Msg
    = StartSlide Direction
    | AnimationFrame Float
    | AddTile Int Int Int
    | NewGame
    | TouchStart Touch.Event
    | TouchEnd Touch.Event
    | TouchCancel Touch.Event
    | ToggleInfo
    | NothingHappened


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        StartSlide direction ->
            ( startSlide direction model, Cmd.none )

        AnimationFrame dt ->
            case model.boardState of
                Sliding slidingState ->
                    updateForSlidingAnimationFrame dt model slidingState

                Static _ ->
                    ( model, Cmd.none )

        AddTile colNum rowNum rank ->
            ( case model.boardState of
                Static { tiles } ->
                    let
                        newTiles =
                            Grid.set colNum rowNum (Just rank) tiles
                    in
                    if anyMovesPossible newTiles then
                        { model | boardState = Static { tiles = newTiles } }

                    else
                        { model
                            | boardState = Static { tiles = newTiles }
                            , gameIsOver = True
                        }

                Sliding _ ->
                    -- Can’t add a tile while tiles are sliding
                    model
            , Cmd.none
            )

        NewGame ->
            ( { model
                | boardState = Static { tiles = initTiles }
                , score = 0
                , gameIsOver = False
              }
            , newTileCmd initTiles
            )

        TouchStart event ->
            ( { model
                | touchModel =
                    coalesceMaybes model.touchModel (eventToTouchModel event)
              }
            , Cmd.none
            )

        TouchEnd event ->
            model.touchModel
                |> Maybe.andThen
                    (\{ identifier, position } ->
                        findFirstInList
                            (\touch -> touch.identifier == identifier)
                            event.changedTouches
                            |> Maybe.andThen
                                (\touch ->
                                    let
                                        ( startX, startY ) =
                                            position

                                        ( endX, endY ) =
                                            touch.clientPos

                                        changeInX =
                                            endX - startX

                                        changeInY =
                                            endY - startY
                                    in
                                    vectorToMaybeDirection
                                        ( changeInX, changeInY )
                                        |> Maybe.map
                                            (\direction ->
                                                ( startSlide direction model
                                                , Cmd.none
                                                )
                                            )
                                )
                    )
                |> Maybe.withDefault
                    ( { model | touchModel = Nothing }, Cmd.none )

        TouchCancel _ ->
            ( { model | touchModel = Nothing }, Cmd.none )

        ToggleInfo ->
            ( { model | infoIsBeingShown = not model.infoIsBeingShown }
            , Cmd.none
            )

        NothingHappened ->
            ( model, Cmd.none )


startSlide : Direction -> Model -> Model
startSlide direction model =
    let
        newBoardState =
            case model.boardState of
                Static { tiles } ->
                    Sliding
                        { tiles = Grid.map rankToSlidingTile tiles
                        , amount = 0.0
                        , step = 0
                        , direction = direction
                        }

                Sliding _ ->
                    model.boardState
    in
    { model | boardState = newBoardState, touchModel = Nothing }


rankToSlidingTile : Maybe Int -> Maybe SlidingTile
rankToSlidingTile tile =
    Maybe.map
        (\rank -> { rank = rank, didCombine = False })
        tile


updateForSlidingAnimationFrame :
    Float
    -> Model
    -> SlidingBoardState
    -> ( Model, Cmd Msg )
updateForSlidingAnimationFrame dt model slidingState =
    let
        { amount, step } =
            slidingState

        newAmount =
            amount + slideAmountIncrement (toFloat step + amount) dt
    in
    if newAmount < 1.0 then
        let
            newBoardState =
                Sliding { slidingState | amount = newAmount }
        in
        ( { model | boardState = newBoardState }, Cmd.none )

    else
        -- We have slid a whole step
        let
            ( newBoardState, theScoreIncrement ) =
                handleSlideStepFinishing newAmount slidingState

            newScore =
                model.score + theScoreIncrement

            newHighScore =
                Basics.max newScore model.highScore
        in
        ( { model
            | boardState = newBoardState
            , score = newScore
            , highScore = newHighScore
          }
        , case newBoardState of
            Sliding _ ->
                Cmd.none

            Static newStaticBoardState ->
                Cmd.batch
                    [ storeHighScore newHighScore
                    , newTileCmd newStaticBoardState.tiles
                    ]
        )


slideAmountIncrement : Float -> Float -> Float
slideAmountIncrement amount dt =
    let
        dAmount =
            dt / (0.5 * (1.0 + sqrt 5.0) * slideTime)
    in
    toFloat (numRows - 1) * (2.0 * amount * dAmount + dAmount ^ 2 + dAmount)


handleSlideStepFinishing : Float -> SlidingBoardState -> ( BoardState, Int )
handleSlideStepFinishing newSlideAmount slidingState =
    let
        { tiles, direction, step } =
            slidingState

        newTiles =
            withTransformedTiles
                direction
                (List.map collapseCol)
                tiles
    in
    if anyTilesWillMove newTiles direction then
        -- This is not the last step
        let
            newBoardState =
                Sliding
                    { tiles = newTiles
                    , amount = newSlideAmount - 1.0
                    , step = step + 1
                    , direction = direction
                    }
        in
        ( newBoardState
        , scoreIncrement tiles newTiles
        )

    else
        -- Sliding is finished
        let
            staticTiles =
                Grid.map (Maybe.map .rank) newTiles
        in
        ( Static { tiles = staticTiles }
        , scoreIncrement tiles newTiles
        )


scoreIncrement : Grid (Maybe SlidingTile) -> Grid (Maybe SlidingTile) -> Int
scoreIncrement prevTiles tiles =
    Grid.sum (Grid.map2 maybeTileScore prevTiles tiles)


maybeTileScore : Maybe SlidingTile -> Maybe SlidingTile -> Int
maybeTileScore maybePrevTile maybeTile =
    case maybeTile of
        Just { rank, didCombine } ->
            if didCombine && not (maybeTileDidCombine maybePrevTile) then
                fibonacci rank

            else
                0

        Nothing ->
            0


maybeTileDidCombine : Maybe SlidingTile -> Bool
maybeTileDidCombine maybeTile =
    Maybe.withDefault False (Maybe.map .didCombine maybeTile)


fibonacci : Int -> Int
fibonacci n =
    case n of
        0 ->
            1

        _ ->
            Tuple.second
                (List.foldr (<|)
                    ( 1, 2 )
                    (List.repeat (n - 1) (\( a, b ) -> ( b, a + b )))
                )


storeHighScore : Int -> Cmd Msg
storeHighScore score =
    setStorage (Encode.object [ ( "highScore", Encode.int score ) ])


anyTilesWillMove : Grid (Maybe SlidingTile) -> Direction -> Bool
anyTilesWillMove grid movementDirection =
    List.any .isMoving
        (Grid.filterToList identity
            (withTransformedTiles movementDirection convertToDisplayTiles grid)
        )


newTileCmd : Grid (Maybe a) -> Cmd Msg
newTileCmd tiles =
    case coordsOfFreeSpaces tiles of
        firstFreeSpace :: otherFreeSpaces ->
            Random.generate (\( col, row, rank ) -> AddTile col row rank)
                (Random.map2
                    (\( col, row ) rank -> ( col, row, rank ))
                    (Random.uniform firstFreeSpace otherFreeSpaces)
                    (Random.int 0 2)
                )

        [] ->
            Cmd.none


coordsOfFreeSpaces : Grid (Maybe a) -> List ( Int, Int )
coordsOfFreeSpaces =
    Grid.indexedFilterToList
        (\col row maybeRank ->
            case maybeRank of
                Just _ ->
                    Nothing

                Nothing ->
                    Just ( col, row )
        )


withTransformedTiles : Direction -> (Grid a -> Grid b) -> Grid a -> Grid b
withTransformedTiles direction f =
    -- f is a function that maps a grid of values to another grid.
    -- This function adapts f to operate on a transformed version of
    -- the grid, where the transformation is given by ‘direction’.
    -- So, whatever direciton f thinks is “up” is mapped to the given
    -- direction.
    case direction of
        Up ->
            f

        Down ->
            Grid.reverseCols >> f >> Grid.reverseCols

        Left ->
            Grid.transpose >> f >> Grid.transpose

        Right ->
            Grid.transpose
                >> Grid.reverseCols
                >> f
                >> Grid.reverseCols
                >> Grid.transpose


type alias DisplayTile =
    { label : Int
    , isMoving : Bool
    }


convertToDisplayTiles : Grid (Maybe SlidingTile) -> Grid (Maybe DisplayTile)
convertToDisplayTiles =
    List.map convertColToDisplayTiles


convertColToDisplayTiles : List (Maybe SlidingTile) -> List (Maybe DisplayTile)
convertColToDisplayTiles col =
    case col of
        tile :: restOfTiles ->
            recurse
                { isMoving = False
                , maybeTile0 = tile
                , remainingTiles = restOfTiles
                , reversedResult = []
                }
                (\{ isMoving, maybeTile0, remainingTiles, reversedResult } ->
                    case remainingTiles of
                        maybeTile1 :: newRemainingTiles ->
                            let
                                ( newIsMoving, tile0IsMoving ) =
                                    case ( maybeTile0, maybeTile1 ) of
                                        ( Just tile0, Just tile1 ) ->
                                            if tilesWillCombine tile0 tile1 then
                                                ( True, False )

                                            else
                                                ( isMoving, isMoving )

                                        _ ->
                                            ( True, isMoving )
                            in
                            Recurse
                                { isMoving = newIsMoving
                                , maybeTile0 = maybeTile1
                                , remainingTiles = newRemainingTiles
                                , reversedResult =
                                    convertToDisplayTile
                                        tile0IsMoving
                                        maybeTile0
                                        :: reversedResult
                                }

                        [] ->
                            Stop
                                (List.reverse
                                    (convertToDisplayTile isMoving maybeTile0
                                        :: reversedResult
                                    )
                                )
                )

        [] ->
            []


convertToDisplayTile : Bool -> Maybe SlidingTile -> Maybe DisplayTile
convertToDisplayTile isMoving =
    Maybe.map
        (\tile ->
            { label = fibonacci tile.rank
            , isMoving = isMoving
            }
        )


collapseCol : List (Maybe SlidingTile) -> List (Maybe SlidingTile)
collapseCol =
    foldPairs collapseTilePair


foldPairs : (a -> a -> ( a, a )) -> List a -> List a
foldPairs f list =
    -- Apply f to each (overlapping) pair of successive elements from
    -- ‘list’.  This is best explained with a diagram:
    -- O─🮢
    -- O──f─🮢
    -- O─────f─🮢
    -- O────────f── result
    case list of
        firstItem :: restOfList ->
            List.foldl
                (\item ( prevItem, newList ) ->
                    let
                        ( nextItem0, nextItem1 ) =
                            f prevItem item
                    in
                    ( nextItem1, nextItem0 :: newList )
                )
                ( firstItem, [] )
                restOfList
                |> (\( last, reversedRest ) -> last :: reversedRest)
                |> List.reverse

        [] ->
            []


collapseTilePair :
    Maybe SlidingTile
    -> Maybe SlidingTile
    -> ( Maybe SlidingTile, Maybe SlidingTile )
collapseTilePair maybeTile0 maybeTile1 =
    case ( maybeTile0, maybeTile1 ) of
        ( Nothing, Just tile1 ) ->
            ( Just tile1, Nothing )

        ( Just tile0, Just tile1 ) ->
            -- If the previous tile had slid, then maybeTile0 would be
            -- Nothing.  Therefore, no tiles to the left have slid.
            if tilesWillCombine tile0 tile1 then
                ( Just
                    { rank = Basics.max tile0.rank tile1.rank + 1
                    , didCombine = True
                    }
                , Nothing
                )

            else
                ( Just tile0, Just tile1 )

        _ ->
            ( maybeTile0, maybeTile1 )


tilesWillCombine : SlidingTile -> SlidingTile -> Bool
tilesWillCombine tile0 tile1 =
    (abs (tile0.rank - tile1.rank) == 1 || tile0.rank == 0 && tile1.rank == 0)
        && not (tile0.didCombine || tile1.didCombine)


anyMovesPossible : Grid (Maybe Int) -> Bool
anyMovesPossible grid =
    let
        slidingTiles =
            Grid.map rankToSlidingTile grid
    in
    [ Left, Right, Up, Down ]
        |> List.map (anyTilesWillMove slidingTiles)
        |> List.any identity


coalesceMaybes : Maybe a -> Maybe a -> Maybe a
coalesceMaybes maybeA maybeB =
    case maybeA of
        Just _ ->
            maybeA

        Nothing ->
            maybeB


eventToTouchModel : Touch.Event -> Maybe TouchModel
eventToTouchModel event =
    case event.changedTouches of
        [] ->
            Nothing

        touch :: _ ->
            Just
                { identifier = touch.identifier
                , position = touch.clientPos
                }



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    if model.gameIsOver then
        onKeyDown
            (Decode.map
                (\keyName ->
                    if keyName == "Enter" then
                        NewGame

                    else
                        NothingHappened
                )
                (Decode.field "key" Decode.string)
            )

    else
        case model.boardState of
            Sliding _ ->
                onAnimationFrameDelta AnimationFrame

            Static _ ->
                onKeyDown keyDecoder


keyDecoder : Decode.Decoder Msg
keyDecoder =
    Decode.map toDirection (Decode.field "key" Decode.string)


toDirection : String -> Msg
toDirection string =
    case string of
        "ArrowLeft" ->
            StartSlide Left

        "a" ->
            StartSlide Left

        "ArrowRight" ->
            StartSlide Right

        "d" ->
            StartSlide Right

        "ArrowUp" ->
            StartSlide Up

        "w" ->
            StartSlide Up

        "ArrowDown" ->
            StartSlide Down

        "s" ->
            StartSlide Down

        _ ->
            NothingHappened


findFirstInList : (a -> Bool) -> List a -> Maybe a
findFirstInList test list =
    case list of
        first :: rest ->
            if test first then
                Just first

            else
                findFirstInList test rest

        [] ->
            Nothing


vectorToMaybeDirection : ( Float, Float ) -> Maybe Direction
vectorToMaybeDirection ( x, y ) =
    let
        isLongEnough =
            -- x and y are in CSS pixels
            max (abs x) (abs y) > 30
    in
    if isLongEnough then
        let
            isCloseEnoughToXAxis =
                abs x > 0 && abs (y / x) < 0.25

            isCloseEnoughToYAxis =
                abs y > 0 && abs (x / y) < 0.25
        in
        if isCloseEnoughToXAxis then
            if x > 0 then
                Just Right

            else
                Just Left

        else if isCloseEnoughToYAxis then
            if y > 0 then
                Just Down

            else
                Just Up

        else
            Nothing

    else
        Nothing



-- VIEW


view : Model -> Html Msg
view model =
    Html.div
        [ Html.Attributes.style "position" "fixed"
        , Touch.onStart TouchStart
        , Touch.onEnd TouchEnd
        , Touch.onCancel TouchCancel
        ]
        (svgView model
            :: (if model.gameIsOver then
                    [ gameOverView ]

                else
                    []
               )
            ++ (if model.infoIsBeingShown then
                    [ infoView ]

                else
                    []
               )
        )


svgView : Model -> Html Msg
svgView model =
    let
        boardRect =
            { x = 0
            , y = 0
            , width = boardSize
            , height = boardSize
            }
    in
    Layout.layOut
        boardMargin
        (boardView boardRect
            ++ sidePanelView
                { x = boardRect.x + boardRect.width + panelMargin
                , y = boardRect.y
                , width = sidePanelWidth
                , height = boardRect.height
                }
                model
            ++ tilesView model.boardState
        )


boardSize : Float
boardSize =
    (toFloat numRows * tileSize) + ((toFloat numRows + 1) * tileMargin)


sidePanelWidth : Float
sidePanelWidth =
    size 8


boardView : Layout.Rect -> List (Layout.Box Msg)
boardView outerRect =
    boardBackgroundView outerRect
        :: slotsView outerRect


boardBackgroundView : Layout.Rect -> Layout.Box Msg
boardBackgroundView outerRect =
    Layout.makeBox outerRect
        |> Layout.rounded (tileMargin + tileCornerRadius)
        |> Layout.withBackground (Layout.FloodColor <| oklch 0.8 0 0)
        |> Layout.withShadow
            { blur = depthToBlur boardDepth
            , extension = boardDepth
            , color = Color.rgba 0 0 0 0.5
            }


slotsView : Layout.Rect -> List (Layout.Box Msg)
slotsView boardRect =
    Grid.map
        (\rect ->
            Layout.makeBox rect
                |> Layout.rounded tileCornerRadius
                |> Layout.withBackground
                    (Layout.VerticalGradient <|
                        reverseGradient <|
                            verticalShadingStops slotColor
                    )
                |> Layout.withForeground
                    (Layout.ContouredGradient <| edgeGradientStops slotColor)
        )
        (Layout.rectGrid boardRect numRows numRows tileMargin tileMargin)
        |> List.concat


sidePanelView : Layout.Rect -> Model -> List (Layout.Box Msg)
sidePanelView outerRect model =
    let
        titleRect =
            { x = outerRect.x
            , y = outerRect.y
            , width = outerRect.width
            , height = tileSize + tileMargin
            }

        highScoreRect =
            { x = outerRect.x + tileSize
            , y = titleRect.y + titleRect.height + tileMargin
            , width = outerRect.width - tileSize
            , height = tileSize
            }

        scoreRect =
            { x = outerRect.x
            , y = highScoreRect.y + highScoreRect.height + tileMargin
            , width = outerRect.width
            , height = tileSize
            }

        numbersRect =
            { x = outerRect.x
            , y = scoreRect.y + scoreRect.height + tileMargin
            , width = outerRect.width - tileSize
            , height = tileSize + tileMargin
            }

        infoButtonRect =
            { x = numbersRect.x + numbersRect.width
            , y = numbersRect.y
            , width = outerRect.width - numbersRect.width
            , height = numbersRect.height
            }

        highestRank =
            List.maximum
                (case model.boardState of
                    Static { tiles } ->
                        Grid.filterToList identity tiles

                    Sliding { tiles } ->
                        Grid.filterToList (Maybe.map .rank) tiles
                )

        highestRankToShow =
            Basics.max 2 (Maybe.withDefault 0 highestRank)
    in
    titleView titleRect
        ++ highScoreView highScoreRect model.highScore
        ++ scoreView scoreRect (highScoreRect.x - scoreRect.x) model.score
        ++ fibonacciView numbersRect highestRankToShow
        ++ infoButtonView infoButtonRect


titleView : Layout.Rect -> List (Layout.Box Msg)
titleView titleRect =
    let
        verticalPadding =
            size 2

        horizontalPadding =
            size 3

        depth =
            size 1

        innerRectHeight =
            titleRect.height - 2 * verticalPadding

        fracasFontSize =
            0.55 * innerRectHeight

        fibonacciFontSize =
            innerRectHeight - fracasFontSize
    in
    [ Layout.makeBox titleRect
        |> Layout.rounded verticalPadding
        |> Layout.withBackground (Layout.FloodColor <| oklch 0.8 0.05 0)
        |> Layout.withShadow
            { blur = depthToBlur depth
            , color = Color.rgba 0 0 0 0.5
            , extension = depth
            }
        |> Layout.withText
            { text = "Fibonacci"
            , fontSize = fibonacciFontSize
            , fontFamily = Layout.Ranchers
            , bold = False
            , opacity = defaultTextOpacity
            , alignmentX = Layout.AlignStart horizontalPadding
            , alignmentY = Layout.AlignStart verticalPadding
            }
        |> Layout.withText
            { text = "Fracas!"
            , fontSize = fracasFontSize
            , fontFamily = Layout.Ranchers
            , bold = False
            , opacity = defaultTextOpacity
            , alignmentX = Layout.AlignEnd horizontalPadding
            , alignmentY = Layout.AlignEnd verticalPadding
            }
    ]


verticalShadingStops : Color -> List ( Float, Color )
verticalShadingStops color =
    [ ( 0, color )
    , ( 0.4 * tileDepth / tileSize, scaleColor 1.5 color )
    , ( tileDepth / tileSize, scaleColor 1.2 color )
    , ( 0.5, color )
    , ( 1.0 - tileDepth / tileSize, scaleColor 0.8 color )
    , ( 1, scaleColor 0.5 color )
    ]


reverseGradient : List ( Float, Color ) -> List ( Float, Color )
reverseGradient stops =
    stops |> List.map (Tuple.mapFirst (\x -> 1 - x)) |> List.reverse


scaleColor : Float -> Color -> Color
scaleColor scale color =
    let
        { red, green, blue, alpha } =
            Color.toRgba color
    in
    Color.rgba (scale * red) (scale * green) (scale * blue) alpha


edgeGradientStops : Color -> List ( Float, Color )
edgeGradientStops color =
    let
        edgeColor =
            CM.darken 0.8 (CM.fadeOut 0.5 color)

        interiorColor =
            CM.fadeOut 1.0 edgeColor
    in
    List.map
        (\x ->
            -- The mixing factor from outer to inner is
            -- x = sin ((pi/2) * offset / (tileDepth / tileCornerRadius))
            -- (so it reaches the inner color at
            -- offset = tileDepth / tileCornerRadius).
            ( asin x / (pi / 2) * tileDepth / tileCornerRadius
            , CI.interpolate CI.RGB edgeColor interiorColor x
            )
        )
        (floatRange 0.0 1.0 5)


floatRange : Float -> Float -> Int -> List Float
floatRange start end numSteps =
    List.range 0 numSteps
        |> List.map (\n -> start + toFloat n * (end - start) / toFloat numSteps)


scoreView : Layout.Rect -> Float -> Int -> List (Layout.Box Msg)
scoreView outerRect split score =
    let
        fontSize =
            0.6 * outerRect.height

        padding =
            size 2

        depth =
            size 1
    in
    [ Layout.makeBox outerRect
        |> Layout.rounded padding
        |> Layout.withBackground (Layout.FloodColor <| oklch 0.8 0.05 320)
        |> Layout.withShadow
            { blur = depthToBlur depth
            , color = Color.rgba 0 0 0 0.5
            , extension = depth
            }
        |> Layout.withText
            { text = String.fromInt score
            , fontSize = fontSize
            , fontFamily = Layout.Ranchers
            , bold = False
            , opacity = defaultTextOpacity
            , alignmentX = Layout.AlignEnd padding
            , alignmentY = Layout.AlignCenter
            }
    , Layout.makeBox { outerRect | width = split }
        |> Layout.withText
            { text = "Score"
            , fontSize = 0.5 * fontSize
            , fontFamily = Layout.Ranchers
            , bold = False
            , opacity = defaultTextOpacity
            , alignmentX = Layout.AlignEnd 0
            , alignmentY = Layout.AlignCenter
            }
    ]


highScoreView : Layout.Rect -> Int -> List (Layout.Box Msg)
highScoreView outerRect score =
    let
        fontSize =
            1 / 3 * outerRect.height

        padding =
            size 3

        depth =
            size 1
    in
    [ Layout.makeBox outerRect
        |> Layout.rounded (size 2)
        |> Layout.withBackground (Layout.FloodColor <| oklch 0.8 0.05 340)
        |> Layout.withShadow
            { blur = depthToBlur depth
            , color = Color.rgba 0 0 0 0.5
            , extension = depth
            }
        |> Layout.withText
            { text = "High Score"
            , fontSize = 0.5 * fontSize
            , fontFamily = Layout.Ranchers
            , bold = False
            , opacity = defaultTextOpacity
            , alignmentX = Layout.AlignCenter
            , alignmentY = Layout.AlignStart padding
            }
        |> Layout.withText
            { text = String.fromInt score
            , fontSize = fontSize
            , fontFamily = Layout.Ranchers
            , bold = False
            , opacity = defaultTextOpacity
            , alignmentX = Layout.AlignCenter
            , alignmentY = Layout.AlignEnd padding
            }
    ]


fibonacciView : Layout.Rect -> Int -> List (Layout.Box Msg)
fibonacciView outerRect highestRank =
    let
        numFibonacciRows =
            4

        rowSize =
            outerRect.height / (numFibonacciRows + 1)

        textSize =
            0.6 * rowSize

        numFibonacciCols =
            4

        innerRect =
            Layout.insetRect (size 2) outerRect

        gridRect =
            { innerRect
                | y = innerRect.y + rowSize
                , height = innerRect.height - rowSize
            }
    in
    (Layout.makeBox outerRect
        |> Layout.rounded (size 2)
        |> Layout.withBackground (Layout.FloodColor <| oklch 0.8 0.05 300)
        |> Layout.withShadow
            { extension = size 2
            , color = Color.rgba 0 0 0 0.5
            , blur = depthToBlur (size 2)
            }
        |> Layout.withText
            { text = "Fibonacci Numbers"
            , fontSize = 1.1 * textSize
            , fontFamily = Layout.Amaranth
            , bold = True
            , opacity = defaultTextOpacity
            , alignmentX = Layout.AlignCenter
            , alignmentY = Layout.AlignStart (size 2)
            }
    )
        :: (Layout.rectGrid gridRect numFibonacciCols numFibonacciRows 0 0
                |> List.concat
                |> List.map2
                    (\rank rect ->
                        Layout.makeBox rect
                            |> Layout.withText
                                { text = String.fromInt (fibonacci rank)
                                , fontSize = textSize
                                , fontFamily = Layout.Amaranth
                                , bold = True
                                , opacity = defaultTextOpacity
                                , alignmentX = Layout.AlignCenter
                                , alignmentY = Layout.AlignEnd 0
                                }
                    )
                    (List.range 0 highestRank)
           )


tilesView : BoardState -> List (Layout.Box Msg)
tilesView boardState =
    case boardState of
        Static { tiles } ->
            Grid.indexedFilterToList
                (\col row ->
                    Maybe.map
                        (\rank ->
                            tileView
                                (toFloat col)
                                (toFloat row)
                                (fibonacci rank)
                        )
                )
                tiles

        Sliding { tiles, amount, direction } ->
            Grid.indexedFilterToList
                (\col row ->
                    Maybe.map
                        (\{ label, isMoving } ->
                            if not isMoving then
                                tileView (toFloat col) (toFloat row) label

                            else
                                let
                                    ( dx, dy ) =
                                        directionToVector direction
                                in
                                tileView
                                    (toFloat col + dx * amount)
                                    (toFloat row + dy * amount)
                                    label
                        )
                )
                (withTransformedTiles direction convertToDisplayTiles tiles)


tileView : Float -> Float -> Int -> Layout.Box Msg
tileView col row label =
    let
        labelSize =
            tileSize / 2.0
    in
    Layout.makeBox
        { x = tileMargin + col * tileSpacing
        , y = tileMargin + row * tileSpacing
        , width = tileSize
        , height = tileSize
        }
        |> Layout.withBackground
            (Layout.VerticalGradient <| verticalShadingStops tileColor)
        |> Layout.rounded tileCornerRadius
        |> Layout.withShadow
            { extension = tileDepth
            , color = Color.rgba 0 0 0 0.5
            , blur = depthToBlur tileDepth
            }
        |> Layout.withText
            { text = String.fromInt label
            , fontSize = labelSize
            , fontFamily = Layout.Ranchers
            , alignmentX = Layout.AlignCenter
            , alignmentY = Layout.AlignCenter
            , bold = False
            , opacity = defaultTextOpacity
            }
        |> Layout.withForeground
            (Layout.ContouredGradient <| edgeGradientStops tileColor)


directionToVector : Direction -> ( Float, Float )
directionToVector direction =
    case direction of
        Up ->
            ( 0.0, -1.0 )

        Down ->
            ( 0.0, 1.0 )

        Left ->
            ( -1.0, 0.0 )

        Right ->
            ( 1.0, 0.0 )


gameOverView : Svg msg
gameOverView =
    let
        gameOverWidth =
            let
                widthOfTiles =
                    toFloat numRows * tileSize

                widthOfTileGaps =
                    toFloat (numRows - 1) * tileMargin
            in
            widthOfTiles + widthOfTileGaps

        gameOverWidthFormula =
            interpolate "calc({0} * {1})"
                [ String.fromFloat (gameOverWidth / boardSize)
                , boardSizeFormula
                ]

        gameOverHeight =
            tileSize + tileMargin

        gameOverHeightFormula =
            interpolate "calc({0} * {1})"
                [ String.fromFloat (gameOverHeight / boardSize)
                , boardSizeFormula
                ]

        gameOverLeftFormula =
            let
                centerXMinusGameOverLeft =
                    svgWidth / 2 - boardMargin - tileMargin
            in
            interpolate "calc(50dvw - {0} * {1})"
                [ String.fromFloat (centerXMinusGameOverLeft / boardSize)
                , boardSizeFormula
                ]

        gameOverTopFormula =
            interpolate "calc(50dvh - {0} * {1})"
                [ String.fromFloat (gameOverHeight / 2 / boardSize)
                , boardSizeFormula
                ]

        textSizeFormula =
            interpolate "calc(0.5 * {0})" [ gameOverHeightFormula ]

        smallTextSizeFormula =
            interpolate "calc(0.12 * {0})" [ gameOverHeightFormula ]
    in
    Html.div
        [ Html.Attributes.style "width" gameOverWidthFormula
        , Html.Attributes.style "height" gameOverHeightFormula
        , Html.Attributes.style "border-radius" "5vmin"
        , Html.Attributes.style "position" "absolute"
        , Html.Attributes.style "left" gameOverLeftFormula
        , Html.Attributes.style "top" gameOverTopFormula
        , Html.Attributes.style "background-color" "#FFFFFF7F"
        , Html.Attributes.style "backdrop-filter" "blur(2vmin)"
        , Html.Attributes.style "text-align" "center"
        ]
        [ Html.div
            [ Html.Attributes.style "font-size" textSizeFormula
            , Html.Attributes.style "font-family" "Ranchers"
            ]
            [ Html.text "Game Over" ]
        , Html.div
            [ Html.Attributes.style "font-family" "Amaranth"
            , Html.Attributes.style "font-size" smallTextSizeFormula
            ]
            [ Html.text "Press "
            , Html.kbd [] [ Html.text "↵Enter" ]
            , Html.text " to begin again"
            ]
        ]


infoView : Svg Msg
infoView =
    let
        infoSize =
            boardSize - 4 * tileMargin

        infoSizeFormula =
            interpolate "calc({0} * {1})"
                [ String.fromFloat (infoSize / boardSize)
                , boardSizeFormula
                ]

        infoLeftFormula =
            let
                centerXMinusInfoLeft =
                    svgWidth / 2 - boardMargin - tileMargin
            in
            interpolate "calc(50dvw - {0} * {1})"
                [ String.fromFloat (centerXMinusInfoLeft / boardSize)
                , boardSizeFormula
                ]

        infoTopFormula =
            interpolate "calc(50dvh - {0} * {1})"
                [ String.fromFloat (infoSize / 2 / boardSize)
                , boardSizeFormula
                ]

        textSizeFormula =
            interpolate "calc(0.03 * {0})" [ boardSizeFormula ]

        paddingFormula =
            interpolate "calc({0} * {1})"
                [ String.fromFloat (tileMargin / boardSize)
                , boardSizeFormula
                ]
    in
    Html.div
        [ Html.Attributes.style "width" infoSizeFormula
        , Html.Attributes.style "height" infoSizeFormula
        , Html.Attributes.style "border-radius" "5vmin"
        , Html.Attributes.style "position" "absolute"
        , Html.Attributes.style "left" infoLeftFormula
        , Html.Attributes.style "top" infoTopFormula
        , Html.Attributes.style "background-color" "#FFFFFF7F"
        , Html.Attributes.style "backdrop-filter" "blur(2vmin)"
        , Html.Attributes.style "overflow" "scroll"
        , Html.Attributes.style "font-size" textSizeFormula
        , Html.Attributes.style "font-family" "Amaranth"
        , Html.Attributes.style "padding" paddingFormula
        ]
        [ Html.p
            []
            [ Html.text
                "Fibonacci Fracas is a game similar to 2048, but using "
            , Html.a
                [ Html.Attributes.href
                    "https://en.wikipedia.org/wiki/Fibonacci_sequence"
                ]
                [ Html.text "Fibonacci numbers" ]
            , Html.text " instead of powers of two."
            ]
        , Html.p
            []
            [ Html.text
                ("Each turn, a new tile with a value of 1, 2, or 3 "
                    ++ "will appear on a random empty space. "
                    ++ "You choose a direction and all the tiles will "
                    ++ "slide in that direction, "
                    ++ "but also adjacent Fibonacci numbers "
                    ++ "that slide into each other "
                    ++ "will combine into a bigger Fibonacci number "
                    ++ "(which is the sum of the two numbers that combined)."
                )
            ]
        , Html.p
            []
            [ Html.text
                ("The game is over when all spaces are full "
                    ++ "and there is no way to make a combination."
                )
            ]
        , Html.h2 [] [ Html.text "Controls" ]
        , Html.dl
            []
            [ Html.dt
                []
                [ Html.kbd [] [ Html.text "←" ]
                , Html.text " or "
                , Html.kbd [] [ Html.text "A" ]
                , Html.text " or swipe left"
                ]
            , Html.dd [] [ Html.text "Slide tiles left" ]
            , Html.dt
                []
                [ Html.kbd [] [ Html.text "→" ]
                , Html.text " or "
                , Html.kbd [] [ Html.text "D" ]
                , Html.text " or swipe right"
                ]
            , Html.dd [] [ Html.text "Slide tiles right" ]
            , Html.dt
                []
                [ Html.kbd [] [ Html.text "↑" ]
                , Html.text " or "
                , Html.kbd [] [ Html.text "A" ]
                , Html.text " or swipe up"
                ]
            , Html.dd [] [ Html.text "Slide tiles up" ]
            , Html.dt
                []
                [ Html.kbd [] [ Html.text "↓" ]
                , Html.text " or "
                , Html.kbd [] [ Html.text "S" ]
                , Html.text " or swipe down"
                ]
            , Html.dd [] [ Html.text "Slide tiles down" ]
            ]
        , Html.h2 [] [ Html.text "Scoring" ]
        , Html.p
            []
            [ Html.text
                ("For each pair of tiles that combine, "
                    ++ "the resulting number is added to your score."
                )
            ]
        , Html.p
            [ Html.Attributes.style "opacity" "0.5"
            , Html.Attributes.style "font-family" "sans-serif"
            ]
            [ Html.span
                [ Html.Attributes.style "font-family" "Ranchers" ]
                [ Html.text "Ranchers" ]
            , Html.text " and "
            , Html.span
                [ Html.Attributes.style "font-family" "Amaranth" ]
                [ Html.text "Amaranth" ]
            , Html.text " fonts are redistributable under the "
            , Html.a
                [ Html.Attributes.href "fonts/ranchers/OFL.txt" ]
                [ Html.text "SIL Open Font License" ]
            ]
        ]


boardSizeFormula : String
boardSizeFormula =
    let
        whenWidthConstrained =
            String.fromFloat (100 * boardSize / svgWidth) ++ "dvw"

        whenHeightConstrained =
            String.fromFloat (100 * boardSize / svgHeight) ++ "dvh"
    in
    interpolate "min({0}, {1})"
        [ whenWidthConstrained, whenHeightConstrained ]


infoButtonView : Layout.Rect -> List (Layout.Box Msg)
infoButtonView rect =
    [ Layout.makeBox rect
        |> Layout.withText
            { text = "🛈"
            , fontFamily = Layout.SansSerif
            , fontSize = (rect.height - 2 * boardMargin) / 2
            , bold = True
            , opacity = 1
            , alignmentX = Layout.AlignEnd 0
            , alignmentY = Layout.AlignEnd 0
            }
        |> Layout.withClickHandler ToggleInfo
    ]


oklch : Float -> Float -> Float -> Color
oklch luminance chroma hueInDegrees =
    Oklch.toColor (Oklch.oklch luminance chroma (hueInDegrees / 360))


svgWidth : Float
svgWidth =
    boardSize + 2 * boardMargin + panelMargin + sidePanelWidth


svgHeight : Float
svgHeight =
    boardSize + 2 * boardMargin


tileSpacing : Float
tileSpacing =
    tileSize + tileMargin


tileMargin : Float
tileMargin =
    size 2


tileSize : Float
tileSize =
    size 6


tileColor : Color
tileColor =
    Color.rgb 0.5 1.0 0.7


slotColor : Color
slotColor =
    Color.rgb 0.75 0.75 0.75


tileCornerRadius : Float
tileCornerRadius =
    size 2


tileDepth : Float
tileDepth =
    size 0


boardDepth : Float
boardDepth =
    tileMargin


boardMargin : Float
boardMargin =
    size 3


defaultTextOpacity : Float
defaultTextOpacity =
    0.75


panelMargin : Float
panelMargin =
    boardMargin


slideTime : Float
slideTime =
    -- In milliseconds
    650.0


infoSymbolSize : Float
infoSymbolSize =
    0.35 * tileSize


depthToBlur : Float -> Float
depthToBlur depth =
    0.3 * depth


size : Int -> Float
size n =
    toFloat (fibonacci n)
