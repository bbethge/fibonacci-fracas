module Layout exposing
    ( Alignment(..)
    , Box
    , FontFamily(..)
    , Image(..)
    , Rect
    , Shadow
    , Text
    , insetRect
    , layOut
    , makeBox
    , rectGrid
    , rounded
    , withBackground
    , withClickHandler
    , withForeground
    , withShadow
    , withText
    )

import Color exposing (Color)
import Grid exposing (Grid)
import Svg.PathD as PathD exposing (pathD)
import TypedSvg as Svg exposing (svg)
import TypedSvg.Attributes as SvgAttr exposing (offset)
import TypedSvg.Core as SvgCore exposing (Svg, attribute)
import TypedSvg.Events as SvgEvent
import TypedSvg.Filters as SvgFilt
import TypedSvg.Filters.Attributes as SvgFiltAttr
import TypedSvg.Types as SvgType exposing (Paint(..), num)


type alias Box msg =
    { rect : Rect
    , cornerRadius : Float
    , maybeBackground : Maybe Image
    , maybeShadow : Maybe Shadow
    , texts : List Text
    , maybeForeground : Maybe Image
    , maybeClickHandler : Maybe msg
    }


type alias Rect =
    { x : Float
    , y : Float
    , width : Float
    , height : Float
    }


insetRect : Float -> Rect -> Rect
insetRect amount rect =
    { x = rect.x + amount
    , y = rect.y + amount
    , width = rect.width - 2 * amount
    , height = rect.height - 2 * amount
    }


type Image
    = FloodColor Color
      -- Gradient stop offsets go from 0 at the top to 1 at the bottom
    | VerticalGradient GradientStops
      -- Gradient stop offsets go from 0 at the outer edge of the box
      -- to 1 at an inset distance equal to the corner radius
    | ContouredGradient GradientStops


type alias GradientStops =
    List ( Float, Color )


type alias Shadow =
    { blur : Float
    , color : Color
    , extension : Float
    }


type alias Text =
    { text : String
    , fontSize : Float
    , fontFamily : FontFamily
    , bold : Bool
    , opacity : Float
    , alignmentX : Alignment
    , alignmentY : Alignment
    }


type FontFamily
    = Ranchers
    | Amaranth
    | SansSerif


type Alignment
    = AlignStart Float
    | AlignCenter
    | AlignEnd Float


makeBox : Rect -> Box msg
makeBox rect =
    { rect = rect
    , cornerRadius = 0
    , maybeBackground = Nothing
    , maybeShadow = Nothing
    , texts = []
    , maybeForeground = Nothing
    , maybeClickHandler = Nothing
    }


rounded : Float -> Box msg -> Box msg
rounded cornerRadius box =
    { box | cornerRadius = cornerRadius }


withBackground : Image -> Box msg -> Box msg
withBackground background box =
    { box | maybeBackground = Just background }


withShadow : Shadow -> Box msg -> Box msg
withShadow shadow box =
    { box | maybeShadow = Just shadow }


withText : Text -> Box msg -> Box msg
withText text box =
    { box | texts = text :: box.texts }


withForeground : Image -> Box msg -> Box msg
withForeground foreground box =
    { box | maybeForeground = Just foreground }


withClickHandler : msg -> Box msg -> Box msg
withClickHandler message box =
    { box | maybeClickHandler = Just message }


type Def
    = VerticalGradientDef GradientStops
    | CornerGradientDef GradientStops
    | BlurFilterDef BlurFilterParams


type alias BlurFilterParams =
    ( Float, Float, Float )


layOut : Float -> List (Box msg) -> Svg msg
layOut margin boxes =
    let
        ( compiledBoxes, defs ) =
            compile boxes

        boundingBox =
            boundingRectOfBoxes boxes
    in
    svg
        [ SvgCore.attribute "width" "100dvw"
        , SvgCore.attribute "height" "100dvh"
        , SvgAttr.viewBox
            (boundingBox.x - margin)
            (boundingBox.y - margin)
            (boundingBox.width + 2 * margin)
            (boundingBox.height + 2 * margin)
        ]
        (Svg.defs
            []
            (List.map (uncurry defToSvg) defs)
            :: compiledBoxes
        )


uncurry : (a -> b -> c) -> ( a, b ) -> c
uncurry f ( x, y ) =
    f x y


boundingRectOfBoxes : List (Box msg) -> Rect
boundingRectOfBoxes =
    List.foldl
        (\box partialBoundingRect ->
            let
                newRect =
                    boundingRectOfBox box

                left =
                    min newRect.x partialBoundingRect.x

                top =
                    min newRect.y partialBoundingRect.y

                right =
                    max (newRect.x + newRect.width)
                        (partialBoundingRect.x + partialBoundingRect.width)

                bottom =
                    max (newRect.y + newRect.height)
                        (partialBoundingRect.y + partialBoundingRect.height)
            in
            { x = left
            , y = top
            , width = right - left
            , height = bottom - top
            }
        )
        -- Start with a point rectangle at the origin.  This works as
        -- long as the desired bounding box contains the origin.
        { x = 0
        , y = 0
        , width = 0
        , height = 0
        }


boundingRectOfBox : Box msg -> Rect
boundingRectOfBox box =
    let
        rect =
            box.rect

        height =
            box.maybeShadow
                |> Maybe.map (\shadow -> rect.height + max 0 shadow.extension)
                |> Maybe.withDefault rect.height
    in
    { rect | height = height }


compile : List (Box msg) -> ( List (Svg msg), List ( Def, String ) )
compile boxes =
    let
        ( layers, defs ) =
            List.foldr
                (\box ( prevLayers, prevDefs ) ->
                    let
                        ( boxLayers, nextDefs ) =
                            compileBox box prevDefs
                    in
                    ( mergeLists boxLayers prevLayers, nextDefs )
                )
                ( [], [] )
                boxes
    in
    ( List.concat layers, defs )


mergeLists : List (List a) -> List (List a) -> List (List a)
mergeLists lists0 lists1 =
    let
        remainder0 =
            List.drop (List.length lists1) lists0

        remainder1 =
            List.drop (List.length lists0) lists1
    in
    List.map2 (++) lists0 lists1 ++ remainder0 ++ remainder1


compileBox :
    Box msg
    -> List ( Def, String )
    -> ( List (List (Svg msg)), List ( Def, String ) )
compileBox box defs =
    let
        ( maybeShadowSvg, updatedDefs1 ) =
            case box.maybeShadow of
                Just shadow ->
                    Tuple.mapFirst Just
                        (compileShadow box.rect box.cornerRadius shadow defs)

                Nothing ->
                    ( Nothing, defs )

        ( maybeBackgroundSvg, updatedDefs2 ) =
            case box.maybeBackground of
                Just backgroundImage ->
                    Tuple.mapFirst Just
                        (compileImage box.rect
                            box.cornerRadius
                            backgroundImage
                            updatedDefs1
                        )

                Nothing ->
                    ( Nothing, updatedDefs1 )

        textSvgs =
            List.map (textToSvg box.rect) box.texts

        ( maybeForegroundSvg, updatedDefs3 ) =
            case box.maybeForeground of
                Just foregroundImage ->
                    Tuple.mapFirst Just
                        (compileImage
                            box.rect
                            box.cornerRadius
                            foregroundImage
                            updatedDefs2
                        )

                Nothing ->
                    ( Nothing, updatedDefs2 )
    in
    ( [ [ Svg.g
            (SvgAttr.transform [ SvgType.Translate box.rect.x box.rect.y ]
                :: (maybeToList box.maybeClickHandler
                        |> List.map SvgEvent.onClick
                   )
                ++ (maybeToList box.maybeClickHandler
                        |> List.map (\_ -> SvgAttr.cursor SvgType.CursorPointer)
                   )
            )
            (maybeToList maybeShadowSvg
                ++ maybeToList maybeBackgroundSvg
                ++ textSvgs
                ++ maybeToList maybeForegroundSvg
            )
        ]
      ]
    , updatedDefs3
    )


maybeToList : Maybe a -> List a
maybeToList maybe =
    case maybe of
        Just thing ->
            [ thing ]

        Nothing ->
            []


compileShadow :
    Rect
    -> Float
    -> Shadow
    -> List ( Def, String )
    -> ( Svg msg, List ( Def, String ) )
compileShadow { width, height } cornerRadius shadow defs =
    let
        shadowHeight =
            height + shadow.extension

        filterDef =
            BlurFilterDef ( width, shadowHeight, shadow.blur )

        filterName =
            case kvlGet filterDef defs of
                Just existingFilterName ->
                    existingFilterName

                Nothing ->
                    "filter" ++ String.fromInt (List.length defs)
    in
    ( Svg.rect
        [ SvgAttr.width (num width)
        , SvgAttr.height (num shadowHeight)
        , SvgAttr.rx (num cornerRadius)
        , SvgAttr.fill (Paint shadow.color)
        , SvgAttr.filter (SvgType.Filter <| "url(#" ++ filterName ++ ")")
        ]
        []
    , ( filterDef, filterName ) :: defs
    )


type alias FilterParams =
    ( Float, Float, Float )


compileImage :
    Rect
    -> Float
    -> Image
    -> List ( Def, String )
    -> ( Svg msg, List ( Def, String ) )
compileImage rect cornerRadius image defs =
    case image of
        FloodColor floodColor ->
            ( svgRect rect cornerRadius (SvgType.Paint floodColor)
            , defs
            )

        VerticalGradient stops ->
            let
                ( gradientName, updatedDefs ) =
                    addVerticalGradient stops defs
            in
            ( svgRect rect cornerRadius (SvgType.Reference gradientName)
            , updatedDefs
            )

        ContouredGradient stops ->
            addContouredGradient rect cornerRadius stops defs


svgRect : Rect -> Float -> SvgType.Paint -> Svg msg
svgRect rect cornerRadius paint =
    Svg.rect
        [ SvgAttr.width (num rect.width)
        , SvgAttr.height (num rect.height)
        , SvgAttr.rx (num cornerRadius)
        , SvgAttr.fill paint
        ]
        []


addVerticalGradient :
    GradientStops
    -> List ( Def, String )
    -> ( String, List ( Def, String ) )
addVerticalGradient stops defs =
    case kvlGet (VerticalGradientDef stops) defs of
        Just gradientName ->
            ( gradientName, defs )

        Nothing ->
            let
                gradientName =
                    "gradient" ++ String.fromInt (List.length defs)
            in
            ( gradientName
            , ( VerticalGradientDef stops, gradientName ) :: defs
            )


addContouredGradient :
    Rect
    -> Float
    -> GradientStops
    -> List ( Def, String )
    -> ( Svg msg, List ( Def, String ) )
addContouredGradient rect cornerRadius stops defs =
    let
        edgeGradientDef =
            VerticalGradientDef stops

        cornerGradientDef =
            CornerGradientDef (reverseGradient stops)

        edgeGradientName =
            case kvlGet edgeGradientDef defs of
                Just defName ->
                    defName

                Nothing ->
                    "gradient" ++ String.fromInt (List.length defs)

        cornerGradientName =
            case kvlGet cornerGradientDef defs of
                Just defName ->
                    defName

                Nothing ->
                    "gradient" ++ String.fromInt (List.length defs + 1)
    in
    ( Svg.g
        []
        ((List.range 0 3
            |> List.map
                (contouredGradientCorner rect cornerRadius cornerGradientName)
         )
            ++ (List.range 0 3
                    |> List.map
                        (contouredGradientEdge
                            rect
                            cornerRadius
                            edgeGradientName
                        )
               )
        )
    , ( edgeGradientDef, edgeGradientName )
        :: ( cornerGradientDef, cornerGradientName )
        :: defs
    )


reverseGradient : GradientStops -> GradientStops
reverseGradient stops =
    stops |> List.map (Tuple.mapFirst (\x -> 1 - x)) |> List.reverse


contouredGradientCorner :
    { a | width : Float, height : Float }
    -> Float
    -> String
    -> Int
    -> Svg msg
contouredGradientCorner rect cornerRadius gradientName cornerNum =
    Svg.path
        [ SvgAttr.fill (SvgType.Reference gradientName)
        , SvgAttr.d <|
            pathD
                [ PathD.M ( cornerRadius, 0 )
                , PathD.A
                    ( cornerRadius, cornerRadius )
                    90
                    False
                    False
                    ( 0, cornerRadius )
                , PathD.L ( cornerRadius, cornerRadius )
                , PathD.Z
                ]
        , SvgAttr.transform
            [ SvgType.Rotate (90 * toFloat cornerNum)
                (rect.width / 2)
                (rect.height / 2)
            ]
        ]
        []


contouredGradientEdge :
    { a | width : Float, height : Float }
    -> Float
    -> String
    -> Int
    -> Svg msg
contouredGradientEdge rect cornerRadius gradientName edgeNum =
    let
        ( width, distanceFromCenter ) =
            if modBy 2 edgeNum == 0 then
                ( rect.width - 2 * cornerRadius, rect.height / 2 )

            else
                ( rect.height - 2 * cornerRadius, rect.width / 2 )
    in
    Svg.rect
        [ SvgAttr.x (num <| rect.width / 2 - width / 2)
        , SvgAttr.y (num <| rect.width / 2 - distanceFromCenter)
        , SvgAttr.width (num width)
        , SvgAttr.height (num cornerRadius)
        , SvgAttr.fill (SvgType.Reference gradientName)
        , SvgAttr.transform
            [ SvgType.Rotate
                (90 * toFloat edgeNum)
                (rect.width / 2)
                (rect.height / 2)
            ]
        ]
        []


fontFamilyToSvg : FontFamily -> List String
fontFamilyToSvg fontFamily =
    case fontFamily of
        Ranchers ->
            [ "Ranchers", "sans-serif" ]

        Amaranth ->
            [ "Amaranth", "sans-serif" ]

        SansSerif ->
            [ "sans-serif" ]


alignmentToSvg : Alignment -> SvgType.AnchorAlignment
alignmentToSvg alignment =
    case alignment of
        AlignStart _ ->
            SvgType.AnchorStart

        AlignCenter ->
            SvgType.AnchorMiddle

        AlignEnd _ ->
            SvgType.AnchorEnd


textToSvg : Rect -> Text -> Svg msg
textToSvg rect positionedText =
    let
        { text, fontSize, fontFamily, bold, opacity, alignmentX, alignmentY } =
            positionedText
    in
    Svg.text_
        [ SvgAttr.fontSize (num fontSize)

        -- We need to line up the center of the cap height with the
        -- center of the rectangle.  We can use cap units, but not
        -- calc, apparently, so we set the y attribute (which is the
        -- baseline y) to 0.5cap so that the text is vertically (and
        -- horizontally) centered on the origin, then use transforms
        -- to move the text to the center (or right side) of the
        -- rectangle.
        , attribute "y"
            (case alignmentY of
                AlignStart _ ->
                    "1cap"

                AlignCenter ->
                    "0.5cap"

                AlignEnd _ ->
                    "0"
            )
        , SvgAttr.transform
            [ SvgType.Translate
                (case alignmentX of
                    AlignStart padding ->
                        padding

                    AlignCenter ->
                        rect.width / 2

                    AlignEnd padding ->
                        rect.width - padding
                )
                (case alignmentY of
                    AlignStart padding ->
                        padding

                    AlignCenter ->
                        rect.height / 2

                    AlignEnd padding ->
                        rect.height - padding
                )
            ]
        , SvgAttr.fontFamily (fontFamilyToSvg fontFamily)
        , if bold then
            SvgAttr.fontWeight SvgType.FontWeightBold

          else
            SvgAttr.fontWeight SvgType.FontWeightNormal
        , SvgAttr.opacity (SvgType.Opacity opacity)
        , SvgAttr.textAnchor (alignmentToSvg alignmentX)
        ]
        [ SvgCore.text text ]


defToSvg : Def -> String -> Svg msg
defToSvg def name =
    case def of
        VerticalGradientDef stops ->
            verticalGradientToSvg stops name

        CornerGradientDef stops ->
            cornerGradientToSvg stops name

        BlurFilterDef params ->
            filterToSvg params name


filterToSvg : FilterParams -> String -> Svg msg
filterToSvg ( width, height, blur ) filterName =
    Svg.filter
        [ SvgAttr.id filterName
        , SvgFiltAttr.filterUnits SvgType.CoordinateSystemUserSpaceOnUse
        , SvgAttr.x (num <| -2 * blur)
        , SvgAttr.y (num <| -2 * blur)
        , SvgAttr.width (num <| width + 4 * blur)
        , SvgAttr.height (num <| height + 4 * blur)
        ]
        [ SvgFilt.gaussianBlur
            [ SvgAttr.stdDeviation (String.fromFloat blur) ]
            []
        ]


verticalGradientToSvg : List ( Float, Color ) -> String -> Svg msg
verticalGradientToSvg stops gradientName =
    Svg.linearGradient
        [ SvgAttr.id gradientName
        , SvgAttr.gradientTransform [ SvgType.Rotate 90 0 0 ]
        ]
        (stopsToSvg stops)


stopsToSvg : GradientStops -> List (Svg msg)
stopsToSvg stops =
    List.map
        (\( offset, color ) ->
            Svg.stop
                [ SvgAttr.offset (String.fromFloat offset)
                , SvgAttr.stopColor (Color.toCssString color)
                ]
                []
        )
        stops


cornerGradientToSvg : List ( Float, Color ) -> String -> Svg msg
cornerGradientToSvg stops gradientName =
    Svg.radialGradient
        [ SvgAttr.id gradientName
        , SvgAttr.cx (num 1)
        , SvgAttr.cy (num 1)
        , SvgAttr.r (num 1)
        ]
        (stopsToSvg stops)


kvlGet : k -> List ( k, v ) -> Maybe v
kvlGet keyToLookUp kvl =
    -- Look up the value for a key in a key-value list (kvl).  This is
    -- useful when you can’t use a Dict because the keys aren’t
    -- comparable.
    kvl
        |> List.filter (\( key, _ ) -> key == keyToLookUp)
        |> List.head
        |> Maybe.map Tuple.second


rectGrid : Rect -> Int -> Int -> Float -> Float -> Grid Rect
rectGrid outerRect numCols numRows padding spacing =
    let
        availableWidth =
            outerRect.width - 2 * padding - (toFloat numCols - 1) * spacing

        availableHeight =
            outerRect.height - 2 * padding - (toFloat numRows - 1) * spacing

        innerRectWidth =
            availableWidth / toFloat numCols

        innerRectHeight =
            availableHeight / toFloat numRows
    in
    List.map
        (\colNum ->
            List.map
                (\rowNum ->
                    { x =
                        outerRect.x
                            + padding
                            + (toFloat colNum * (innerRectWidth + spacing))
                    , y =
                        outerRect.y
                            + padding
                            + (toFloat rowNum * (innerRectHeight + spacing))
                    , width = innerRectWidth
                    , height = innerRectHeight
                    }
                )
                (List.range 0 (numRows - 1))
        )
        (List.range 0 (numCols - 1))
