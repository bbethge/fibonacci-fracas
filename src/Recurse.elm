module Recurse exposing (Recursion(..), recurse)

-- This is another way to write loops in a functional language.


type Recursion a b
    = Recurse a
    | Stop b


recurse : a -> (a -> Recursion a b) -> b
recurse initVal f =
    case f initVal of
        Recurse nextVal ->
            recurse nextVal f

        Stop finalVal ->
            finalVal
